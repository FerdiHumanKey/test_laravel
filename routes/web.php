<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'getAllPost'])->name('home');


Route::get('/posts',[UserController::class,'getAllPost'])->name('getAllPost');

Route::get('/posts/{id}',[UserController::class,'getPostId'])->name('getPostId'); //l'id nn richiede il dollaro

Route::get('/add-post',[UserController::class,'addPost'])->name('add-post');

Route::get('/update-post',[UserController::class,'updatePost'])->name('update-post');

Route::get('/delete-post/{id}',[UserController::class,'deletePost'])->name('delete-post'); //l'id nn richiede il dollaro

Route::get('/xml', [UserController::class,'getXML'])->name('xml');

Route::get('/testXmlToJson', [UserController::class,'getXMLtoJson'])->name('xmlToJson');

Route::get('/testGuzzle', [UserController::class,'getUrl'])->name('xmlURL');

Route::get('/getXMLPacchetto', [UserController::class,'getXMLPacchetto'])->name('getXMLPacchetto');