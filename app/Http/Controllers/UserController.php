<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class UserController extends Controller
{
    public function getAllPost()
    {
        $collection = Http::get('https://jsonplaceholder.typicode.com/posts/');
        return $collection->json();
    }

    public function getPostId($id)
    {
        $post = Http::get('https://jsonplaceholder.typicode.com/posts/'.$id);
        return $post->json();
    }

    public function addPost()
    {
        $post = Http::post('https://jsonplaceholder.typicode.com/posts/',[
            'userId' => 3,
            'title' => 'test',
            'body' => 'test body'
        ]);
        return $post->json();
    }

    public function updatePost()
    {
        $response = Http::put('https://jsonplaceholder.typicode.com/posts/1',[
            'title' => 'aggiorna titolo',
            'body' => 'aggiorna corpo messaggio'
        ]);
        return $response->json();
    }

    public function deletePost($id)
    {
        $response = Http::delete('https://jsonplaceholder.typicode.com/posts/1'.$id);
        return $response->json();
    }

    public function getXML()
    {
        $xml = 'https://www.w3schools.com/xml/cd_catalog.xml';
        $simpleXml = simplexml_load_file($xml);
        $xmlToJson = json_encode($simpleXml);
        $jsonToArray = json_decode($xmlToJson ,TRUE);
        $jsonToArrayCD =  $jsonToArray['CD'];
        $json = json_encode($jsonToArrayCD);
        // dd($jsonToArrayCD);
        return $json;

    }
        public function getXMLtoJson()
    {
        $xml = 'https://www.w3schools.com/xml/cd_catalog.xml';
        $simpleXml = simplexml_load_file($xml);
        $xmlToJson = json_encode($simpleXml);
        $jsonToArray = json_decode($xmlToJson ,TRUE);
        // dd($jsonToArray);
        $jsonToArrayCD =  $jsonToArray['CD'];
        // dd($jsonToArrayCD);
        return view('testXmlToJson' , compact('jsonToArrayCD'));
        return $simpleXml;
    }

    public function getUrl()
    {
        $xml = 'https://www.w3schools.com/xml/cd_catalog.xml';
        $client = new \GuzzleHttp\Client();
        $request = $client->get($xml);
        $response = $request->getBody();
        // dd($response);
        echo '<pre>';
        return $response;
        echo '</pre>';
    }

    public function getXMLPacchetto()
    {
        // $xml = Http::get('http://new.kisskiss.it/xml/RispostaGetPacchetto.xml');
        $xml = 'http://new.kisskiss.it/xml/RispostaGetPacchetto.xml';
        $simpleXml = simplexml_load_file($xml);
        $xmlToJson = json_encode($simpleXml);
        $jsonToArray = json_decode($xmlToJson ,TRUE);
        $jsonToArrayCD =  $jsonToArray['Risposta'];
        $json = json_encode($jsonToArrayCD);
        // dd($jsonToArrayCD);
        return $json;

    }
}
