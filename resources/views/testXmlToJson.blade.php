<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Test xml to Json</title>
</head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">test xml to json</h2>
                    <h3 class="text-center">I cd più acqusitati di sempre</h3>
                </div>
            </div>
            <div class="row">
                @foreach ($jsonToArrayCD as $key => $jsonData)
                <div class="col-lg-3 col-sm-2 col-md-4">
                        <ul class="list-group mb-2">
                            <li class="list-group-item">Titolo: {{$jsonData['TITLE']}}</li>
                            <li class="list-group-item">Autore: {{$jsonData['ARTIST']}}</li>
                            <li class="list-group-item">Città: {{$jsonData['COUNTRY']}}</li>
                            <li class="list-group-item">Azienda: {{$jsonData['COMPANY']}}</li>
                            <li class="list-group-item">Prezzo: {{$jsonData['PRICE']}}</li>
                            <li class="list-group-item">Anno: {{$jsonData['YEAR']}}</li>
                          </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </body>
</html>